#  📻 Reportaje multimedia 🗞

==================================================================================================

## DESCRIPCIÓN:

Voces del silencio es un reportaje multimedia 🎥 sobre la violencia de género en el Ecuador 🇪🇨, fue realizado en 2016. 

Si quieres contactarme para hacer sugerencias 🤩 puedes hacer un pull request o escribirme a annacha@inf.upv.es


==================================================================================================


### AGRADECIMIENTOS
A las mujeres que alzan su voz, a las que acompañan, a las que están en el camino del encuentro con ellas mismas.

